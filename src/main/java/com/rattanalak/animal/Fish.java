/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.animal;

/**
 *
 * @author Rattanalak
 */
public class Fish extends AquaticAnimal {
    private String nickname;
    private String color;
    public Fish(String nickname, String color) {
        super("Fish",0);
        this.nickname = nickname;
        this.color = color;
    }

    @Override
    public void swim() {
        System.out.println("Fish :" + nickname + " swim");
    }

    @Override
    public void eat() {
        System.out.println("Fish :" + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Fish :" + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Fish's name :" + nickname + ". Color : " + color);
        System.out.println("Fish :" + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Fish :" + nickname + " sleep");
    }
    
    
}
