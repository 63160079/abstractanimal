/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.animal;

/**
 *
 * @author Rattanalak
 */
public abstract class AquaticAnimal extends Animal{
    
    public AquaticAnimal(String name, int numOfleg) {
        super(name, numOfleg);
    }
    
    public abstract void swim();
}
