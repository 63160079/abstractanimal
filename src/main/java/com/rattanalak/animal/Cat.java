/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.animal;

/**
 *
 * @author Rattanalak
 */
public class Cat extends LandAnimal{
    private String nickname;
    private String color;

    public Cat(String nickname,String color) {
        super("Cat", 4);
        this.nickname = nickname;
        this.color = color;
    }

    @Override
    public void run() {
        System.out.println("Cat : "+ nickname + " run");
    }

    @Override
    public void eat() {
        System.out.println("Cat : "+ nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Cat : "+ nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Cat's name : " + nickname + ". Color : " + color );
        System.out.println("Cat : "+ nickname + " speak > Meow!!!");
    }

    @Override
    public void sleep() {
        System.out.println("Cat : "+ nickname + " sleep");
    }
    
}
