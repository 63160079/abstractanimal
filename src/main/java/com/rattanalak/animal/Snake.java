/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.animal;

/**
 *
 * @author Rattanalak
 */
public class Snake extends Reptile {
    private String nickname;
    private String color;
    
    public Snake(String nickname, String color) {
        super("snake", 0);
        this.nickname = nickname;
        this.color = color;
    }

    @Override
    public void crawl() {
        System.out.println("Snake : " + nickname + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Snake : " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Snake : " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Snake's name : " + nickname + ". Color : " + color );
        System.out.println("Snake : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Snake : " + nickname + " sleep");
    }
    
}
