/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.animal;

/**
 *
 * @author Rattanalak
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        newLine();
        Dog d1 = new Dog("Lek", "BlackWhite");
        d1.eat();
        d1.speak();
        newLine();
        Cat c1 = new Cat("Khaw", "White");
        c1.speak();
        c1.sleep();
        newLine();
        Alligator l1 = new Alligator("Yak");
        l1.crawl();
        l1.eat();
        Snake s1 = new Snake("Luem", "WhiteYellow");
        s1.speak();
        s1.sleep();
        s1.crawl();
        newLine();
        Fish f1 = new Fish("Som", "orange");
        f1.speak();
        f1.swim();
        newLine();
        Crab r1 = new Crab("Cheang");
        r1.swim();
        r1.walk();
        newLine();
        Bird b1 = new Bird("Kaw", "Red");
        b1.speak();
        b1.fly();
        newLine();
        Bat t1 = new Bat("Dum");
        t1.fly();
        t1.sleep();
        newLine();
        
        Animal[] animals = {h1, d1, c1, l1, s1, f1, r1, b1, t1};
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i] + " is Animal? : " + (animals[i] instanceof Animal));
            System.out.println(animals[i] + " is LandAnimal? : " + (animals[i] instanceof LandAnimal));
            System.out.println(animals[i] + " is Reptile Animal? : " + (animals[i] instanceof Reptile));
            System.out.println(animals[i] + " is Aquatic Animal? : " + (animals[i] instanceof AquaticAnimal));
            System.out.println(animals[i] + " is Poultry? : " + (animals[i] instanceof Poultry));
            newLine();
        }
    }

    private static void newLine() {
        System.out.println("-----------------------------");
    }
}
