/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.animal;

/**
 *
 * @author Rattanalak
 */
public class Bird extends Poultry{
    private String nickname;
    private String color;
    
    public Bird(String nickname, String color) {
        super("Bird", 2);
        this.nickname = nickname;
        this.color = color;
    }

    @Override
    public void fly() {
        System.out.println("Bird : " + nickname + " fly");
    }

    @Override
    public void eat() {
       System.out.println("Bird : " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Bird : " + nickname + " walk");
    }

    @Override
    public void speak() {
                System.out.println("Bird's : " + nickname + ". Color : " + color);
        System.out.println("Bird : " + nickname + " speak > jib jib!!!");
    }

    @Override
    public void sleep() {
        System.out.println("Bird : " + nickname + " sleep");
    }
    
}
